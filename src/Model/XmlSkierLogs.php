<?php
/**
  * This file is a part of the code used in IMT2571 Assignment 5.
  *
  * @author Rune Hjelsvold
  * @version 2018
  */

require_once('Club.php');
require_once('Skier.php');
require_once('YearlyDistance.php');
require_once('Affiliation.php');

/**
  * The class for accessing skier logs stored in the XML file
  */  
class XmlSkierLogs
{
    /**
      * @var DOMDocument The XML document holding the club and skier information.
      */  
    protected $doc;
    
    /**
      * @param string $url Name of the skier logs XML file.
      */  
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
		$this->xpath = new DOMXpath($this->doc);
    }
    
    /**
      * The function returns an array of Club objects - one for each
      * club in the XML file passed to the constructor.
      * @return Club[] The array of club objects
      */
    public function getClubs()
    {
        $clubs = array();
        $elements = $this->xpath->query('/SkierLogs/Clubs/Club');
		foreach ($elements as $element) {
			$Name = $element->getElementsByTagName("Name");
			$City = $element->getElementsByTagName("City");
			$County = $element->getElementsByTagName("County");
			$valueName = $Name->item(0)->nodeValue;
			$valueCity = $City->item(0)->nodeValue;
			$valueCounty = $County->item(0)->nodeValue;
			$value = new Club($element->getAttribute('id'), 
			$valueName, $valueCity, $valueCounty);
			array_push($clubs, $value);
		}
        // TODO: Implement the function retrieving club information
        return $clubs;
    }

    /**
      * The function returns an array of Skier objects - one for each
      * Skier in the XML file passed to the constructor. The skier objects
      * contains affiliation histories and logged yearly distances.
      * @return Skier[] The array of skier objects
      */
  public function getSkiers() {
    $skiers = array();
	$elements = $this->xpath->query('/SkierLogs/Skiers/Skier');
	foreach ($elements as $element) {
		$firstName = $element->getElementsByTagName("FirstName");
		$lastName = $element->getElementsByTagName("LastName");
		$yearOfBirth = $element->getElementsByTagName("YearOfBirth");
		$valueFName = $firstName->item(0)->nodeValue;
		$valueLName = $lastName->item(0)->nodeValue;
		$valueYOB = $yearOfBirth->item(0)->nodeValue;
		$value = new Skier($element->getAttribute('userName'), 
		$valueFName, $valueLName, $valueYOB);
			
		$seasons = $this->xpath->query('/SkierLogs/Season');
		foreach ($seasons as $season) { 
			foreach ($season->getElementsByTagName("Skiers") as $affElement) { 
				foreach ($affElement->getElementsByTagName("Skier") as $skierElement) { 
					if ($skierElement->getAttribute('userName') == $element->getAttribute('userName')) {
						// Adds the club and season as an affiliation of the skier
						
						if ($affElement->getAttribute('clubId')){
						$affiliation = new Affiliation($affElement->getAttribute('clubId'), $season->getAttribute('fallYear'));
						$value->addAffiliation($affiliation);
						}
													
						// The log elements
						foreach($skierElement->getElementsByTagName('Log') as $log) { 
							$distance = array();
							// The log entries
							foreach ($log->getElementsByTagName('Entry') as $entry) { 
								$xmlDistance = $entry->getElementsByTagName("Distance");
								$distance[] = $xmlDistance->item(0)->nodeValue;
							}
						}
						// Adds the distance
						$yearlyDistance = new YearlyDistance($season->getAttribute('fallYear'),array_sum($distance));
						$season->getAttribute('fallYear');
						$value->addYearlyDistance($yearlyDistance);
						//$value->addAffiliation($affiliation);

						//$value->addYearlyDistance(array_sum($distance), $season->getAttribute('fallYear')); 
					}
				}
			}
		}
	array_push($skiers, $value);
	}
       
    // TODO: Implement the function retrieving skier information,
    //       including affiliation history and logged yearly distances.
    return $skiers;
    }
}

?>